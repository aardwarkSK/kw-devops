# Kafka workshop - DevOps

## SSH Push

Run `ssh_push.sh` to push docker images to remote machine over ssh.
Pre Requisites: SSH configured


# Docker

This folder contains Docker configuration for Kafka workshop. All development environments are configured here.

There is one single docker-compose.yaml.

All environment specific configurations are placed in subfolders named after the environment (e.g. localhost to run locally).


## Configuration structure

- infrastructure.env - contains docker-compose specific variables
- [service].env - contains configurations for docker container used via [env_file](https://docs.docker.com/compose/compose-file/#env_file) option


## Check configurations

`docker-compose --env-file [PATH] config`


## Start containers

`docker-compose --env-file [PATH] -p [project name e.g local] up`


## Update single container

`docker-compose --env-file prod/infrastructure.env -p prod up -d --no-deps`


## Environments

**Local**: `docker-compose --env-file local/infrastructure.env -p local up -d`

**Prod**: `docker-compose --env-file prod/infrastructure.env -p prod up -d`
