#!/bin/bash

ssh_host=${docker_host_user}"@"${docker_host}

scp -r ./docker $ssh_host:$kw_root_dir

ssh $ssh_host -T "source ~/.bash_profile; cd "$zoho_root_dir"/docker; docker-compose --env-file ./prod/infrastructure.env pull; docker-compose --env-file ./prod/infrastructure.env -p prod up -d --no-deps"