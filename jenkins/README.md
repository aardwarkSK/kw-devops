# Jenkins deploy setup

## Jenkins enviroment

### Plugins:
* ssh agent 
    - set credentials to build context

### KW jenkins project variables file:
Add to **Config File Management** property file: *docker_env* with variables:
* docker_host 
    - host/ip of machine to deploy
* docker_user
    - user of machine to deploy
* kw_root_dir
    - root dir on target with kw-docker env files

### Credentials
Add to project credential key for host machine - ssh key 

## Setup build project:

#### Source Code Management
* setup git & authorization

#### Build Environment
* Delete workspace before build starts
* Provide Configuration files
    - File: 
        - *docker_env*
* Inject environment variables to the build process
    - Property File Path: 
        - *${docker_env}*
* SSH Agent
    - select credential to host machine

#### Build
* In execute shell script add: 
    
    ```
    bash -ex ./jenkins/scripts/deploy.sh
    ```
  
## Notes
#### docker-compose --context
Not used because of [issue](https://github.com/docker/compose/issues/7434) where you cannot set docker context to only one docker-compose command.
Solution we cannot use because changing the context to docker client will change context to all running jobs of jenkins. 

#### ssh environment variables
When command is passed to ssh `ssh user@host -T "echo $VARIABLE""` .bash_profile is ignored - which contains our env variables 
which means you need to execute `source ~/.bash_profile` whenever **anything** in command uses env variables 